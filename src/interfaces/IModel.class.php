<?php

namespace Apart\Interfaces;

interface IModel {
    /**
     * @return IDataStorage
     */
    public function getStorage();

    /**
     * @return array
     */
    public function attributeList();
}