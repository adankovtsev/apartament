<?php

use Apart\Classes\MapCity;
use Apart\Classes\MapCost;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

/** Инициализация основных объектов */
MapCity::init();
MapCost::init();

/** @var $input - получем доступ к входному потоку данных*/
$input = fopen('php://stdin', 'r');

while ($row = fgets($input)) {
    /** exit - команда выхода */
    if (trim($row) == 'exit') break;
    $parts = explode(',', $row);

    if (count($parts) >= 2) {
        /** Функция принимает только числа */
        $regionId = preg_replace('/[^\d]/s', '', $parts[0]);
        $square = preg_replace('/[^\d]/s', '', $parts[1]);

        $result = MapCost::get($regionId, $square);
        echo ($result === false ? 'false' : $result) . PHP_EOL;
    }
}

fclose($input);