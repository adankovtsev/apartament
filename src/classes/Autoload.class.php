<?php

namespace Apart\Classes;

use Apart\Exceptions\ApartClassNotFoundException;

require_once SRC_PATH . 'exceptions' . DS . 'ApartClassNotFoundException.class.php';

class Autoload
{
    /**
     * Автозагрузка
     * @param $className
     * @throws ApartClassNotFoundException
     */
    public static function autoload($className)
    {
        $parts = explode('\\', $className);

        $rootName = array_shift($parts);

        if ($rootName === 'Apart') {
            $path = SRC_PATH;
            $class = array_pop($parts);

            if (!empty($parts)) {
                foreach ($parts as $part) {
                    $path .= strtolower($part) . DS;
                }
            }

            $path .= $class . '.class.php';

            if (is_readable($path)) {
                require_once $path;
            } else {
                throw new ApartClassNotFoundException("Class \"{$className}\" not found");
            }
        }
    }
}