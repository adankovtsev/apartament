<?php

namespace Apart\Interfaces;

interface ICost {
    /**
     * Площадь ОТ
     * @return integer
     */
    public function getFrom();

    /**
     * Площадь ДО
     * @return integer
     */
    public function getTo();

    /**
     * Стоимость квадратного метра
     * @return integer
     */
    public function getCost();
}