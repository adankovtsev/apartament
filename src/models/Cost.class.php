<?php

namespace Apart\Models;

use Apart\Classes\CsvDataStorage;
use Apart\Classes\Model;
use Apart\Interfaces\ICost;

class Cost extends Model implements ICost
{
    /**
     * Хранилище
     * @return CsvDataStorage
     */
    public function getStorage()
    {
        return CsvDataStorage::connect(ROOT_PATH . DS . 'tmp' . DS . 'cost.csv');
    }

    /**
     * Допустимые атрибуты
     * @return array
     */
    public function attributeList()
    {
        return array(
            's_from',
            's_to',
            'cost',
            'regionId'
        );
    }

    /**
     * Цена квадратного метра
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Площадь ОТ
     * @return integer
     */
    public function getFrom()
    {
        return $this->s_from;
    }

    /**
     * Площадь ДО
     * @return integer
     */
    public function getTo()
    {
        return $this->s_to;
    }
}