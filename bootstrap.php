<?php

defined('ROOT_PATH') or define('ROOT_PATH', dirname(__FILE__));
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
defined('SRC_PATH') or define('SRC_PATH', ROOT_PATH . DS . 'src' . DS);

require_once ROOT_PATH . DS . 'src' . DS . 'classes' . DS . 'Autoload.class.php';

spl_autoload_register(array('\\Apart\\Classes\\Autoload', 'autoload'));