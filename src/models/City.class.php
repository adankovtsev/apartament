<?php

namespace Apart\Models;

use Apart\Classes\CsvDataStorage;
use Apart\Classes\Model;

class City extends Model
{
    /**
     * Хранилище
     * @return CsvDataStorage
     */
    public function getStorage()
    {
        return CsvDataStorage::connect(ROOT_PATH . DS . 'tmp' . DS . 'city.csv');
    }

    /**
     * Доступные атрибуты
     * @return array
     */
    public function attributeList()
    {
        return array(
            'name',
            'type',
            'id',
            'parentId',
        );
    }
}