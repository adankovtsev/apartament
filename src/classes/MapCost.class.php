<?php

namespace Apart\Classes;

use Apart\Models\Cost;
use Apart\Interfaces\ICost;

class MapCost
{
    private static $pool;

    /**
     * Добавление информации по стоимости в пул
     * @param $regionId
     * @param ICost $cost
     */
    public static function add($regionId, ICost $cost)
    {
        self::$pool[$regionId][] = $cost;
    }

    /**
     * Получение средней стоимости квартиры в регионе $regionId площадью $square
     * @param $regionId
     * @param $square
     * @return bool
     */
    public static function get($regionId, $square)
    {
        /** @var ICost $curCost */
        $curCost = self::checkRegion($regionId, $square);

        if (is_null($curCost)) {
            return false;
        }

        return $square * $curCost->getCost();
    }

    /**
     * Получение объекта Cost подходящего условию $regionId и площади $square
     * @param $regionId
     * @param $square
     * @return ICost|null
     */
    protected static function checkRegion($regionId, $square)
    {
        $city = MapCity::get($regionId);

        if (is_null($city)) {
            return null;
        }

        $curCost = null;

        if (array_key_exists($regionId, self::$pool)) {
            /** @var ICost $cost */
            foreach (self::$pool[$regionId] as $cost) {
                if ($cost->getFrom() <= $square && $cost->getTo() >= $square) {
                    $curCost = $cost;
                    break;
                }
            }
        }

        if (is_null($curCost)) {
            $parentRegionId = MapCity::get($regionId);
            $curCost = self::checkRegion($parentRegionId->parentId, $square);
        }

        return $curCost;
    }

    /**
     * Инициализация пула стоимостей
     */
    public static function init()
    {
        foreach (Cost::create()->findAll() as $cost) {
            self::add($cost->regionId, $cost);
        }
    }
}