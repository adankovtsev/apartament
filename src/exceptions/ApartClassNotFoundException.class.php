<?php

namespace Apart\Exceptions;

class ApartClassNotFoundException extends \Exception {}