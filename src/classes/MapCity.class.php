<?php

namespace Apart\Classes;

use Apart\Models\City;

class MapCity
{
    private static $pool;

    /**
     * @param $id
     * @param City $city
     */
    public static function set($id, City $city)
    {
        self::$pool[$id] = $city;
    }

    /**
     * @param $id
     * @return City|null
     */
    public static function get($id)
    {
        if (!array_key_exists($id, self::$pool)) {
            return null;
        }

        return self::$pool[$id];
    }

    /**
     * Инициализация пула городов
     */
    public static function init()
    {
        foreach (City::create()->findAll() as $city) {
            self::set($city->id, $city);
        }
    }
}