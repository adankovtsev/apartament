<?php

namespace Apart\Classes;

use Apart\Interfaces\IModel;

abstract class Model implements IModel
{
    protected $_attributes;

    public static function create()
    {
        return new static();
    }

    /**
     * Функция возвращает значение атрибута
     * @param $key
     * @return null
     */
    public function __get($key)
    {
        if (!in_array($key, $this->attributeList())) {
            return null;
        }

        return array_key_exists($key, $this->_attributes) ? $this->_attributes[$key] : null;
    }

    /**
     * Функция устанавливает значение атрибута
     * @param $key
     * @param $value
     */
    public function __set($key, $value)
    {
        if (in_array($key, $this->attributeList())) {
            $this->_attributes[$key] = $value;
        }
    }

    /**
     * Функция инициирует атрибуты из массива
     * @param array $row
     * @return $this
     */
    public function import(array $row)
    {
        $attrs = $this->attributeList();

        foreach ($row as $key => $value) {
            $this->{$attrs[$key]} = $value;
        }

        return $this;
    }

    /**
     * Функция извлекает из хранилища объекты и создаёт экземпляры класса Model
     * @return array
     */
    public function findAll()
    {
        $list = array();
        $storage = $this->getStorage();
        $storage->open();

        while ($row = $storage->fetch()) {
            $model = new static();
            $list[] = $model->import($row);
        }

        $storage->close();
        return $list;
    }
}