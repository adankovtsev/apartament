<?php

namespace Apart\Classes;

class CsvDataStorage extends DataStorage
{
    private $path;
    private $handle;

    /**
     * Подключению к хранилищу
     * @param string $path
     * @return CsvDataStorage
     */
    public static function connect($path = '')
    {
        return new self($path);
    }

    private function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Открыть доступ к хранилищу
     */
    public function open()
    {
        $this->handle = fopen($this->path, 'r+');
    }

    /**
     * Закрыть хралищие
     */
    public function close()
    {
        fclose($this->handle);
    }

    /**
     * Извлечение данных из хранилища в виде массива
     * @return array
     */
    public function fetch()
    {
        return fgetcsv($this->handle, null, ';');
    }
}