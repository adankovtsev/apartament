<?php

namespace Apart\Interfaces;

interface IDataStorage {
    /**
     * Инициализация хранилища
     * @return mixed
     */
    public static function connect();

    /**
     * Открытие доступа к хранилищу
     * @return mixed
     */
    public function open();

    /**
     * Закрытие доступа к хранилищу
     * @return mixed
     */
    public function close();

    /**
     * Получение строки данных из хранилища
     * @return mixed
     */
    public function fetch();
}